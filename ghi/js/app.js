function createCard(name, description, pictureUrl, starts, ends, city) {
    return `
    <div class="grid gap-0 row-gap-3 column-gap-10">
    <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle">${city}</h6>
        <p class="card-text">${description}</p>
        <div class="card-footer text-muted">
        <p>${starts.toLocaleDateString()}-${ends.toLocaleDateString()}</p>
        </div>
        </div>
        </div>
    </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);

    if (!response.ok){

    } else {
        const columns = document.querySelectorAll('.col');
        const data = await response.json();
        let columnIndex = 0;

    for (let conference of data.conferences){
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok){
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const starts = new Date(details.conference.starts);
            const ends = new Date(details.conference.ends);
            const location = details.conference.location.city;

            const pictureUrl = details.conference.location.picture_url;
            const html = createCard(name, description, pictureUrl, starts, ends, location);
            // const column = document.querySelector('.col');
            // column.innerHTML += html;
            columns[columnIndex].innerHTML += html;
            columnIndex = (columnIndex + 1) % 3;
    }
    }

    }
} catch (e) {
    console.error(e);

}

});
